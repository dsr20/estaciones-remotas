import java.io.*;
import java.rmi.*;

public class clienteRMI {
    

    /** Creates a new instance of cliente_rmi */
    public clienteRMI() {
    }

    private int pedirNumeros(int operacion, int resultado, InterfazRemoto objetoRemoto)
    {
        objetoRemoto.suma(op1,op2);
                }
                else
                {
                        if (operacion == 2)
                        {
                                resultado = objetoRemoto.multiplicacion(op1,op2);
                        }
                }
            }
            catch(Exception exc)
            {
                System.out.println("Error al realizar la operacion "+exc);
            }
            return resultado;
    }

    private void pedirOperacion(String host, String port)
    {
            InterfazRemoto objetoRemoto = null;
            InputStreamReader ent = new InputStreamReader(System.in);
            BufferedReader buf = new BufferedReader(ent);
            
            String servidor = "rmi://"+host+":"+port+"/ObjetoRemoto";
            try
            {
            	System.setSecurityManager(new RMISecurityManager());            	
                objetoRemoto = (InterfazRemoto) Naming.lookup(servidor);
            }
            catch(Exception ex)
            {
                System.out.println("Error al instanciar el objeto remoto "+ex);
                System.exit(0);
            }
        
            objetoRemoto = null;
            return;
    }
    private void menu(String host, String port)
    {
        pedirOperacion(host,port);
    }    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
	String host;
        String port;
        clienteRMI cr = new clienteRMI();
	int i = 0;
	if (args.length < 2) {
        try
		{
	        System.out.println("Debe indicar la direccion del servidor");
		}
        catch(Exception e)
		{
        	System.out.println("Error: "+e);
		}
		return;
	}
	host = args[0];
        port = args[1];
	while(i==0)
	{
		cr.menu(host,port);
	}
    }
}