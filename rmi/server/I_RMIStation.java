import java.rmi.Remote;

public interface I_RMIStation extends Remote {
	public void creArchivo() throws java.rmi.RemoteException;
	
	public String contenidoFichero () throws java.rmi.RemoteException;
	public void actualizaFromFichero() throws java.rmi.RemoteException;
	public void decideSiExiste() throws java.rmi.RemoteException;

	public int getTemperatura() throws java.rmi.RemoteException;
	public int getHumedad() throws java.rmi.RemoteException;
	public int getLuminosidad() throws java.rmi.RemoteException;
	public String getPantalla() throws java.rmi.RemoteException;

	public void setTemperatura(int temperatura) throws java.rmi.RemoteException;
	public void setHumedad(int humedad) throws java.rmi.RemoteException;
	public void setLuminosidad(int luminosidad) throws java.rmi.RemoteException;
	public void setPantalla(String pantalla) throws java.rmi.RemoteException;

}