import java.io.Serializable;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.*;
import java.io.FileWriter;
import java.rmi.*;
import java.rmi.server.*;

public class RMIStation extends UnicastRemoteObject
implements I_RMIStation, Serializable {
	private int nEstacion;
	private int temperatura;
	private int humedad;
	private int luminosidad;
	private String pantalla;

	public RMIStation (int nEstacion) throws RemoteException {
		super();
		this.nEstacion = nEstacion;
		temperatura = 30;
		humedad = 90;
		luminosidad = 450;
		pantalla = "Hola, esta es la practica de SD";

		decideSiExiste(); // LLAMAMOS AL METODO QUE CREA EL ARCHIVO O REEMPLAZA LOS VALORES
	}

	public int getTemperatura() throws RemoteException { 
		//actualizaFromFichero();
		return temperatura;
	}
	public int getHumedad() throws RemoteException { 
		//actualizaFromFichero();
		return humedad;
	}
	public int getLuminosidad() throws RemoteException {
		//actualizaFromFichero(); 
		return luminosidad;
	}
	public String getPantalla() throws RemoteException { 
		//actualizaFromFichero();
		return pantalla;
	}

	public void setTemperatura(int temperatura) throws RemoteException { 
		this.temperatura=temperatura;
		creArchivo();
	}
	public void setHumedad(int humedad) throws RemoteException { 
		this.humedad=humedad;
		creArchivo();
	}
	public void setLuminosidad(int luminosidad) throws RemoteException { 
		this.luminosidad=luminosidad;
		creArchivo();
	}
	public void setPantalla(String pantalla) throws RemoteException { 
		this.pantalla=pantalla;
		creArchivo();
	}

	public void actualizaFromFichero() {
		File archivo =  null;
		FileReader fr = null;
		BufferedReader bf = null;

		try {
			archivo = new File("estacion"+nEstacion+".txt");
			fr = new FileReader (archivo);
			bf = new BufferedReader(fr);
			String str = "";
			int n = 1;
			while((str=bf.readLine())!=null) {
				if(n==1)
					temperatura = Integer.parseInt(str.split("=")[1]);
				if(n==2)
					humedad = Integer.parseInt(str.split("=")[1]);
				if(n==3)
					luminosidad = Integer.parseInt(str.split("=")[1]);
				if(n==4)
					pantalla = str.split("=")[1];
				
				n++;
			}			
		}
		catch(Exception e){
			e.printStackTrace();
		}finally {

			try{
				bf.close();
				fr.close();
			}catch (IOException e2){ 
				e2.printStackTrace();
			}
		}
	}
	public void decideSiExiste() {
		File archivo = new File("estacion"+nEstacion+".txt");

		if(archivo.exists())
			actualizaFromFichero();
		else
			creArchivo();
	}
	public void creArchivo() {
		File archivo =  null;
		FileWriter fr = null;
		BufferedWriter bw = null;
		try {

			archivo = new File ("estacion"+nEstacion+".txt");
			// SI NO EXISTE EL ARCHIVO LO CREAMOS
			if(!archivo.exists()) {
				archivo.createNewFile();
			}

			fr = new FileWriter (archivo);
			bw = new BufferedWriter(fr);
			bw.write(contenidoFichero());
			bw.close();
			fr.close();
			

		}
		catch(Exception e){
			e.printStackTrace();
		}

		// SI EL ARCHIVO EXISTE ACTUALIZAMOS DESDE EL FICHERO LOS DATOS DE LA STACION
		/*try {
			archivo = new File ("estacion"+nEstacion+".txt");
			if(archivo.exists()) {
				actualizaFromFichero();
			}
		} catch(RemoteException e) {
			System.out.println(e.toString());
		}
		*/
	}
	public String contenidoFichero () {
		String datos = "";

		datos += "Temperatura="+temperatura+"\n";
		datos += "Humedad="+humedad+"\n";
		datos += "Luminosidad="+luminosidad+"\n";
		datos += "Pantalla="+pantalla+"\n";

		return datos;
	}
}