import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.net.*;
import java.io.*;
import java.util.Scanner;

public class Registro {       
    private static int nEstacion=0;

    public Registro(){
        nEstacion++;
    }  
    public int getNStacion(){ return nEstacion;}

    public static void main (String args[])     
    {    
        String entradaTeclado = "";
        System.out.println("Indique cuantas estaciones desea desplegar: ");

        // OBTENEMOS EL NUMERO DE ESTACIONES A DESPLEGAR
        Scanner entradaEscaner = new Scanner (System.in); //Creación de un objeto Scanner
        entradaTeclado = entradaEscaner.nextLine ();

        int n = Integer.parseInt(entradaTeclado);    // PASAMOS A INT

        for(int i=0;i<n;i++){
            String URLRegistro;
            try           
            {   
                Registro registro = new Registro();
                System.setSecurityManager(new RMISecurityManager());
                RMIStation rmiStation = new RMIStation(registro.getNStacion());                  
                URLRegistro = "/RMIStation"+registro.getNStacion();
                Naming.rebind (URLRegistro, rmiStation);            
                System.out.println("Servidor de objeto preparado.");
            }            
            catch (Exception ex)            
            {                  
                System.out.println(ex);            
            }     
        }
    }
}
