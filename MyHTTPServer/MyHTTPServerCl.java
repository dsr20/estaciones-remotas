import java.io.*;
import java.net.*;
import java.util.*;
import java.lang.Exception;

public class MyHTTPServerCl extends Thread {

	private Socket socketClient;
	private String dirController;

	public MyHTTPServerCl (Socket skCliente, String dirController) {

		this.socketClient = skCliente;
		this.dirController = dirController;

		System.out.println("Nuevo Usuario conectado");
	
	}

	public Socket getSocket() { return socketClient; }

	public void run() {
		String request = "";
		String response = "";
		try {
			request = escucha(socketClient);

			response = parseRequest(request); // PARSEAMOS LA PETICION HTTP

			//System.out.println(request);
			//devuelveIndex(); // DEVOLVEMOS LA PAGINA INDEX
			escribe(getSocket(),response);	
			socketClient.close();	
	
		}catch(IOException e) {
			e.toString();
		}
	}

	public String parseRequest(String request){
		System.out.println(request);
		String str = request.substring(0, request.indexOf("\n")); //CAPTURAMOS LA PRIMERA LINEA DE LA PETICION

		String words[] = str.split(" ");	// DIVIDIMOS LA LINEA (PETICION GET) EN PALABRAS				

		// COMPROBAMOS QUE RECIBIMOS UNA PETICION GET
		if(!words[0].equals("GET")) {
			// DEVOLVEMOS ERROR 405
			return addHeaders("405 Method Not Allowed",0);
		}

		// DEVOLVEMOS INDEX.HTML SI NO SE ESPECIFICA NADA
		if(words[1].equals("/")) {
			return devuelveRecurso(new File("index.html"));
		}

		// VOLVEMOS A PARSER LA URL PARA SEPARAR LOS RECURSOS DE LA PETICION
		String url[] = words[1].split("/");

		// COMPROBAMOS QUE SE LLAMA A UN RECURSO DINAMICO
		if(url[1].equals("controllerSD")){
			// CREAMOS OBJETO CONTROLLER QUE SE ENCARGA DE RECIBIR LA PETICION Y DEVOLVER EL RECURSO
			// ENVIAMOS EL RECURSO AL USUARIO
			return callController(url[2]);		// LLAMAMOS AL CONTROLLER
		}

		File mifichero = new File(url[1]);

		// COMPROBAMOS SI EXISTE EL RECURSO
		if(!mifichero.exists()) {
			// DEVOLVEMOS ERROR 404 
			return addHeaders("404 Not Found",0);
		}
		else {
			return devuelveRecurso(mifichero);
		}

		//return "HTTP/1.1 400 Bad Request\n";
	}

	public String callController(String request) {
		// PARSEAMOS LA DIRECCION DEL CONTROLLER YA QUE NOS LLEGA UNA STRING IP:PUERTO
		String response = "";
		String html = "";
		String params[] = dirController.split(":");
		try {
			Socket contrSockt = new Socket(params[0],Integer.parseInt(params[1])); // CREAMOS EL SOCKET PARA COMUNICARNOS CON EL CONTROLLER
			escribe(contrSockt,request);
			
			html = escuchaPru(contrSockt,html);
			response += addHeaders("200 ok",html.length());
			response += html;
			
			//System.out.println(response);
		}catch(UnknownHostException e) {
			System.out.println(e.toString());
		}catch(IOException e){
			System.out.println(e.toString());
		}

		System.out.println(response);
		return response;
	}	

	// METODO PARA AÑADIR LAS CABECERAS
	public String addHeaders(String httpCode,long lngth) {
		String datos = "";

		datos += "HTTP/1.1 "+ httpCode +"\n";
		datos += "Connection: close\n";
		datos += "Server: MyHTTPServer/1.0\n";
		datos += "Content-Type: text/html; charset=utf-8\n";
		datos += "Content-Length: " + lngth +"\n";
		datos += "\n";	// LINEA EN BLANCO

		return datos;
	}
 
	public String devuelveRecurso(File miFichero) {

		String datos = "";

		if (miFichero.exists()) 
	    {
			datos += addHeaders("200 OK",miFichero.length()); // AÑADIMOS LAS CABECERAS A LA RESPUESTA
			
			try {

				// CLASE QUE NOS AYUDA A LEER CADA LINEA DEL FICHERO
				BufferedReader ficheroLocal = new BufferedReader(new FileReader(miFichero));

    			String linea = ficheroLocal.readLine();

    			while(linea!=null) {
    				datos += linea + "\n";
    				linea = ficheroLocal.readLine();
    			}

			} catch(IOException e) {
				e.toString();
			}
			
		} 
		else
		{
	  		datos += "HTTP/1.0 400 ok\n";
	  		//out.close();
		}
		//escribe(getSocket(),datos);
		return datos;
	}

	public String escuchaPru (Socket p_sk,String p_Datos){
		try {
			InputStream aux = p_sk.getInputStream();
			DataInputStream flujo = new DataInputStream(aux);
			p_Datos = flujo.readUTF();
		}catch(Exception e){
			e.toString();
		}
		return p_Datos;
	}

	/*public String escucha (Socket p_sk,String p_Datos){
		try {
			InputStream aux = p_sk.getInputStream();
			DataInputStream flujo = new DataInputStream(aux);
			p_Datos = flujo.readUTF();
		}catch(Exception e){
			e.toString();
		}
		return p_Datos;
	}*/

	public String escucha(Socket clienteSock) {

		String p_Datos = "";
		BufferedReader in = null;

		try {

			in = new BufferedReader( new InputStreamReader(clienteSock.getInputStream()));
			//BufferedReader stdIn = new BufferedReader( new InputStreamReader(System.in));

        	String linea = in.readLine();

        	while (!linea.equals("") ) {

            	p_Datos += linea +"\n";
            	linea = in.readLine();
			}

		}catch(IOException e) {
			System.out.println(e.toString());

		}

      return p_Datos;
	}

	public void escribe(Socket clienteSock,String datos) {

		try {

			OutputStream aux = clienteSock.getOutputStream(); // RECIBIMOS EL DESCRIPTOR
			DataOutputStream flujo = new DataOutputStream(aux);

			String str;
			//Scanner scan = new Scanner(System.in);

//			str = scan.nextLine();
			//System.out.println(datos);
			flujo.writeUTF(datos); 
		} 
		catch (IOException e){
			System.out.println(e.toString());
		}
	}
	


	/*public static void main(String[] args) {

		String puerto =  args[0];

		if(args.length<1) {

			System.out.println("Error: Falta un argumento"); 
			System.exit(-1);
		}

		Servidor servidor = new Servidor(puerto);

		// INICIAMOS EL CHAT
		servidor.inicia();
	}*/


}