import java.io.*;
import java.net.*;
import java.util.Scanner;

public class MyHTTPServerMst {

	private ServerSocket socket; 
	private final static int maxConnections = 100;
	// Al crear un objeto iniciamos el socket

	public MyHTTPServerMst(String puerto) {

		try {

			this.socket = new ServerSocket(Integer.parseInt(puerto));

			System.out.println("Conectado al puerto "+puerto);
		}
		catch (IOException e) {

			System.out.println("Error: " + e.toString());
		}

	}

	public ServerSocket getSocket() { return socket; }


	public String escucha(Socket clienteSock) {
		String p_Datos = "";
		try
		{
			InputStream aux = clienteSock.getInputStream();

			DataInputStream flujo = new DataInputStream( aux );
			p_Datos = new String();
			p_Datos = flujo.readUTF();

		}
		catch (Exception e)
		{
			System.out.println("Error: " + e.toString());
		}

      return p_Datos;
	}

	public void inicia(String dirController) {
		
		try {

			for(;;) {

				Socket skCliente =  getSocket().accept(); // ACEPTAMOS LA PETICION 
				System.out.println("Sirviendo cliente...");

				Thread hilo =  new MyHTTPServerCl(skCliente,dirController); // CREAMOS UN NUEVO HILO DE EJECUCION PARA SOPORTAR AL NUEVO CLIENTE
				hilo.start();
				//skCliente.close(); 
			}
		}catch(IOException e) {
			System.out.println(e.toString());
		}

		
		
	}

	public static void main(String[] args) {

		String puerto =  args[0];
		String dirContrllr = args[1];

		if(args.length<1) {

			System.out.println("Error: Falta un argumento"); 
			System.exit(-1);
		}

		MyHTTPServerMst servidor = new MyHTTPServerMst (puerto);

		// INICIAMOS EL CHAT
		servidor.inicia(dirContrllr);
	}
}