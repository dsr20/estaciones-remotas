import java.io.*;
import java.net.*;
import java.util.*;
import java.rmi.*;

public class Controller {
	private ServerSocket serverSkt;
	private String host;
	private String port;

	public Controller (String puerto, String dirRmi) {
		try {

			this.serverSkt = new ServerSocket(Integer.parseInt(puerto));
			String str[] = dirRmi.split(":");
			this.host = str[0];
			this.port = str[1];

		}
		catch (IOException e) {

			System.out.println("Error: " + e.toString());
		}
	}

	public void inicia() {

		for(;;) {
			try {

				Socket skCliente =  serverSkt.accept(); // ACEPTAMOS LA PETICION 
				System.out.println("Entrando Peticion...");
				String requestRMI = escucha(skCliente);
				System.out.println(requestRMI);

				// PARSEAMOS LA PETICION
				parseRequest(requestRMI,skCliente);
				//conectaRMI(requestRMI,skCliente);
				//String str = escucha(skCliente);
				//System.out.println(str);
	
			}
			catch(IOException e) {
				System.out.println(e.toString());
			}
		}	
	}

	public void parseRequest(String requestRMI,Socket skCliente) {
		String nParametro,recurso;
		Boolean modRecurso = false;

		// EN CASO DE QUE NO EXISTA EL CARACTER ?
		if(requestRMI.indexOf('?')==-1) {

			// PUEDE INDICAR INDEX
			if(requestRMI.equals("index")){
				escribe(skCliente,buildIndex());
			}
			else {		// PETICION MAL ESCRITA ERROR
			}
		// EL CARACTER ? EXISTE	
		} else {
			String words[] = requestRMI.split("\\?");
			recurso = words[0];

			// COMPROBAMOS SI EN LA PETICION NOS ENTRA UN GET O UN SET
			// ENCONTRAR EL CARACTER '=' EN ESTA PARTE INDICARIA QUE ES UN SET
			if(recurso.indexOf('=')!=-1) {
				nParametro = recurso.split("=")[1];
				recurso = recurso.split("=")[0];
				modRecurso = true; // ESTA VARIABLE INDICA SI SE VA A MODIFICAR EL RECURSO
			}

			String estacion = words[1];

			// COMPROBAMOS SI LA PETICION DE LA ESTACION ESTA BIEN ESCRITA
			if(estacion.indexOf("=")!=-1) {
				words = estacion.split("=");

				// COGEMOS EL NOMBRE DE LA ESTACION
				estacion = "RMIStation"+words[1];
				// LLAMAMOS AL OBJETO RMI
				conectaRMI(estacion,skCliente,recurso,modRecurso); 
			}
			else { // ERROR

			}
		}
	}

	public void conectaRMI (String estacion,Socket skCliente,String recurso,Boolean modRecurso) {
		I_RMIStation objetoRemoto = null;
        InputStreamReader ent = new InputStreamReader(System.in);
        BufferedReader buf = new BufferedReader(ent);
        
        String servidor = "rmi://"+host+":"+port+"/"+estacion;
        String valor = "";
        try
        {
        	System.setSecurityManager(new RMISecurityManager());            	
            objetoRemoto = (I_RMIStation) Naming.lookup(servidor);

            if(modRecurso)
            	valor = setRecurso(recurso,nParametro,objetoRemoto);
            else
            	valor = getRecurso(recurso,objetoRemoto);
        }
        catch(NotBoundException e) {
        	valor = "Error: La estacion solicitada no existe "+e;
        }
        catch(RemoteException e) {
        	valor = "Error: No se puede conectar con la estacion "+e;
        }
        catch(MalformedURLException e) {
        	valor = "Error: La URL no esta bien escrita"+e;
        }
    	escribe(skCliente,buildHtml(recurso,valor));
        objetoRemoto = null;
	}

	public String setRecurso(String recurso, String valor,I_RMIStation objetoRemoto) {
		String str = "";
		try {
			if(recurso.equals("temperatura")) {
				objetoRemoto.setTemperatura(Integer.parseInt(valor));
				str = Integer.toString(objetoRemoto.getTemperatura());
			} else if(recurso.equals("humedad")) {
				objetoRemoto.setHumedad(Integer.parseInt(valor));
				str = Integer.toString(objetoRemoto.getHumedad());
			} else if(recurso.equals("luminosidad")) {
				objetoRemoto.setLuminosidad(Integer.parseInt(valor));
				str = Integer.toString(objetoRemoto.getLuminosidad());
			} else if(recurso.equals("pantalla")) {
				objetoRemoto.setPantalla(valor);
				str = objetoRemoto.getPantalla();
			} else
	        	str = "ERROR: No existe el recurso";
	    } catch(RemoteException e) {
	        	System.out.println("Error: No se puede conectar con la estacion "+e);
	    }
		
		return str;
	}

	public String getRecurso(String recurso,I_RMIStation objetoRemoto){
		String str = "";
		try {
			if(recurso.equals("temperatura"))
	        	str = Integer.toString(objetoRemoto.getTemperatura());	
	    	else if(recurso.equals("humedad"))
	    		str = Integer.toString(objetoRemoto.getHumedad());
	    	else if(recurso.equals("luminosidad"))
	    		str = Integer.toString(objetoRemoto.getLuminosidad());
	    	else if(recurso.equals("pantalla"))
	    		str = objetoRemoto.getPantalla();
	    	else
	    		return "ERROR: No existe el recurso";
	     } catch(RemoteException e) {
	        	System.out.println("Error: No se puede conectar con la estacion "+e);
	    }
	    return str;
	}

	public String buildHtml(String recurso,String valor) {
		String html = "<HTML>\n";
		html += "<HEAD> <TITLE>Servidor de recursos dinamicos</TITLE></HEAD\n";
		html += "<BODY><h2>"+recurso+"</h2>: "+valor+"</BODY>\n";
		html += "</BODY>\n</HTML>";

		return html;
	}

	public String buildIndex() {
		I_RMIStation objetoRemoto = null;

        InputStreamReader ent = new InputStreamReader(System.in);
        BufferedReader buf = new BufferedReader(ent);
        
        String valor = "";
        try
        {
            String html = "<HTML>\n";
			html += "<HEAD> <TITLE>Servidor de recursos dinamicos</TITLE></HEAD\n";
			html += "<BODY>\n";
			for(int i=1;i<=3;i++){
				String estacion = "RMIStation"+i;

        		String servidor = "rmi://"+host+":"+port+"/"+estacion;
        		System.setSecurityManager(new RMISecurityManager());            	
            	objetoRemoto = (I_RMIStation) Naming.lookup(servidor);

				html += "<h2>"+estacion+"</h2>";
				html += "<p><b>Humedad</b>"+objetoRemoto.getHumedad()+"</p>";
				html += "<p><b>Temperatura</b>"+objetoRemoto.getTemperatura()+"</p>";
				html += "<p><b>Luminosidad</b>"+objetoRemoto.getLuminosidad()+"</p>";
				html += "<p><b>Pantalla</b>"+objetoRemoto.getPantalla()+"</p>";
			}
			html += "</BODY>\n";
			html += "</BODY>\n</HTML>";	
            
        }
        catch(NotBoundException e) {
        	valor = "Error: La estacion solicitada no existe "+e;
        }
        catch(RemoteException e) {
        	valor = "Error: No se puede conectar con la estacion "+e;
        }
        catch(MalformedURLException e) {
        	valor = "Error: La URL no esta bien escrita"+e;
        }
		

		return html;
	}

	public String escucha(Socket clienteSock) {
		String p_Datos = "";
		try
		{
			InputStream aux = clienteSock.getInputStream();

			DataInputStream flujo = new DataInputStream( aux );
			p_Datos = new String();
			p_Datos = flujo.readUTF();

		}
		catch (Exception e)
		{
			System.out.println("Error: " + e.toString());
		}

      return p_Datos;
	}

	public void escribe(Socket skt,String datos) {

		try {

			OutputStream aux = skt.getOutputStream(); // RECIBIMOS EL DESCRIPTOR
			DataOutputStream flujo = new DataOutputStream(aux);
			//System.out.println(datos);
			flujo.writeUTF(datos); 
		} 
		catch (IOException e){
			System.out.println(e.toString());
		}
	}

	public static void main(String args[]) {

		String puerto =  args[0];
		String dirContrllr = args[1];

		if(args.length<1) {

			System.out.println("Error: Falta un argumento"); 
			System.exit(-1);
		}

		Controller cntrll = new Controller(args[0],args[1]);
		cntrll.inicia();
	}
}